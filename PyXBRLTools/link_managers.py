class CalculationLinkManager:
    def __init__(self, xbrl_zip_path):
        ...

    def to_cal_link_df(self):
        ...


class DefinitionLinkManager:
    def __init__(self, xbrl_zip_path):
        ...

    def to_def_link_df(self):
        ...


class PresentationLinkManager:
    def __init__(self, xbrl_zip_path):
        ...

    def to_pre_link_df(self):
        ...
